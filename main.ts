import * as process from "process";
import * as fs from "fs";

interface MapData {
    nodes: Array<{ label: string }>,
    edges: Array<{ source: number, target: number, cost: number }>
}

interface Route {
    cost: number,
    target: Node
}

interface Node {
    label: string,
    connectedNodes: Array<Route>
}

interface Path {
    target: Node,
    costs: number,
    path: Array<Node>
}

interface NodesMap {
    [key: string]: Node;
}

function buildMap(mapData: MapData) {
    const nodesMap = mapData.nodes.reduce((nodesMap, node) => {
        nodesMap[node.label] = Object.assign(node, { connectedNodes: Array<Route>() }) as Node;
        return nodesMap;
    }, {} as NodesMap);

    mapData.edges.forEach(edge => {
        const source = mapData.nodes[edge.source] as Node;
        const target = mapData.nodes[edge.target] as Node;
        source.connectedNodes.push({ cost: edge.cost, target: target });
        target.connectedNodes.push({ cost: edge.cost, target: source });
    });

    return nodesMap;
}

function searchPathWidth(originNode: Node, targetNode: Node): Path | undefined {
    let current: Path | undefined = {target: originNode, costs: 0, path: []};
    const visitedNodes: Array<Path> = [];
    let foundNodes: Array<Path> = [];

    do {
        const { costs, path, target } = current;

        const alreadyVisitedIdx = visitedNodes.findIndex(value => value.target === target);

        if(alreadyVisitedIdx >= 0) {

            const alreadyVisited: Path = visitedNodes[alreadyVisitedIdx];
            if(alreadyVisited.costs > current.costs) {

                visitedNodes.splice(alreadyVisitedIdx, 1);
            } else {

                continue;
            }
        }

        visitedNodes.push(current);

        if(target === targetNode) {
            continue;
        }

        const newFoundNodes: Array<Path> = target.connectedNodes.map(value => ({
            target: value.target,
            costs: costs + value.cost,
            path: [...path, target]
        }));

        foundNodes = foundNodes.filter(value => !newFoundNodes.some(value1 => value1.target === value.target));
        foundNodes.push(...newFoundNodes);

    } while ((current = foundNodes.shift()) !== undefined);

    return visitedNodes.find(value => value.target === targetNode);
}

function main(filepath: string) {
    const fileContent = fs.readFileSync(filepath, { encoding: "utf8" });
    const mapData = JSON.parse(fileContent) as MapData;

    const nodesMap = buildMap(mapData);

    const EarthNode = nodesMap["Erde"];
    const b3r7r4nd7 = nodesMap["b3-r7-r4nd7"];

    console.log();
    console.log("search started");
    const path = searchPathWidth(EarthNode, b3r7r4nd7);

    console.debug(path);
    if (path) {
        console.log('Costs:', path.costs);
        let nodes = path.path.reduce((previousValue, currentValue) => {
            return previousValue + currentValue.label + ' -> ';
        }, '') + path.target.label;
        console.log('Path:', nodes);
    } else {
        console.log('no path found');
    }


    console.log();
    console.log("finish");
}

main(process.argv[2]);